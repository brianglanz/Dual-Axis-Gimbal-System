# Open Source Dual Axis Gimbal System

[![License](https://img.shields.io/badge/License-GPL-lightgray.svg?style=flat-square)](https://gitlab.com/brianglanz/Dual-Axis-Gimbal-System/-/blob/master/LICENSE)

<img  title="Dual Axis Gimbal System" src="/images/001.jpg" alt="Dual Axis Gimbal System" width="500" height="452">

*Figure: An open source gimbal system for transmission measurement. A set of aligned fiber optic cables are shown secured with cable ties, with a sample mounted on the rotating gimbal stage.*
## Highlights
- Low-cost, 3-D printable, open source, dual axis gimbal system
- Arduino-based microcontroller, used to move over two degrees of freedom
- Unidirectional accuracy of 2.827°, repeatability of 1.585°, and backlash error of 1.237°
- Maximum speed of 35.156° per second and verifiable microstep size of 0.33°
- 96% cost in savings compared to the least expensive commercial variant
## Purpose of the machine
Simple, 3-D printable, open source hardware designs have proven to be effective scientific instruments at low costs.

One research area where low-cost technology is needed is to characterize thin film anti-reflective coatings and transparent conducting oxides (TCOs) for glass, mirror, and solar photovoltaics whose transmission properties are angle dependent — a need met by a sufficiently low-cost dual axis gimbal system.
## How the machine works
By coupling open source electronics with 3-D printable mechanical components, we can make fully functional and distributedly-manufactured mechatronic tools for science.

The design methodology used for this device is a Free and Open Source Hardware (FOSH) optimization model comprised of these principles:
1. use a free and open source tool chain
1. minimize the number and type of parts
1. minimize the complexity of the tools
1. minimize the amount of material and cost of production
1. maximize the use of components that can be 3-D printed
1. create parametric designs to enable design customization and use of off-the-shelf parts i.e. readily available throughout the world

In this gimbal system, an Arduino-based microcontroller moves the sample holder to the user-specified angle. There, two stepper motors control the motion, providing two degrees of freedom. The sample holder is made so that samples can be easily mounted by two movable latches.
### Functional diagrams
<img  title="Schematic assembling all 3-D printed parts" src="/images/002.png" alt="Schematic assembling all 3-D printed parts" width="500" height="650">

*Schematic showing assembly of all 3-D printed parts. The sample stage, motor, and circuit assembly parts were constructed before joining them to the supporting fiber optic alignment components.*

<img  title="Schematic showing placement, axes of rotation, and optical path" src="/images/003.png" alt="Schematic showing placement, axes of rotation, and optical path" width="500" height="231">

*Schematic showing placement of the gimbal system equidistant from a fiber optic light source and detector, with axes of rotation and the optical path highlighted in red.*

<img  title="Circuit schematic with interconnects between microcontroller, power, and drivers" src="/images/004.png" alt="Circuit schematic with interconnects between microcontroller, power, and drivers" width="500" height="310">

*Circuit schematic of open source gimbal system showing interconnects between Arduino microcontroller, power supply, and drivers.*

The system prompts the user to choose a terminal x-y coordinate or a trace between the initial and user-provided final x-y coordinates. These can be changed with code. [Pseudocode for the Gimbal system included here](/Code/pseudocode.txt) describes the stepper motor control and the following flowchart describes the programming logic.

<img  title="Schematic showing microcontroller logic flowchart" src="/images/005.png" alt="Schematic showing microcontroller logic flowchart" width="500" height="841">

*Flowchart showing logic used by the microcontroller to move the major and minor platforms on y and x axes, respectively.*

## Validation
The mechatronic system was tested for the intended application using a halogen light source and a spectrometer, to measure transmission through glass TCO samples, through a hemisphere.
### Results
Despite the highest mean squared errors, the gimbal system performed as expected while measuring transmission of radiation through glass with TCO coatings. The system was validated and characterized as having:
- unidirectional accuracy of 2.827°
- repeatability of 1.585°
- backlash error of 1.237°
- maximum speed resolution of of 35.156°
- verifiable microstep size of 0.33°

### Qualifications
This open source system represents a 96% savings in cost when compared to the least expensive commercial variant. High mean squared errors are offset by the cost of the system, coupled with its open source nature which encourages collaboration and further development.

## Historical overview
Free and open source technological development decreases ambiguity and makes information freely available to a large group of developers[^1]<sup>,</sup>[^2]<sup>,</sup>[^3]. Higher modularization[^4] in open technology allows for collaboration in which knowledge is shared efficiently by many[^5]<sup>,</sup>[^6].
[^1]:Laat, P.B. de, 2007b. Governance of open source software: state of the art. J Manage Governance 11, 165–177. doi:10.1007/s10997-007-9022-9
[^2]:Grant, R.M., 1996. Toward a knowledge-based theory of the firm. Strat. Mgmt. J. 17, 109–122. doi:10.1002/smj.4250171110
[^3]:Simon, H.A., 1991. Organizations and Markets. The Journal of Economic Perspectives 5, 25–44.
[^4]:Narduzzo, A. and Rossi, A., 2008. _Modularity in action: GNU/Linux and free/open source software development model unleashed_ (No. 020). Department of Computer and Management Sciences, University of Trento, Italy.
[^5]:Meyer, M.H., Seliger, R., 1998. Product Platforms in Software Development. Sloan Management Review; Cambridge 40, 61–74.
[^6]:Allen, E.B., Khoshgoftaar, T.M. and Chen, Y., 2001. Measuring coupling and cohesion of software modules: an information-theory approach. In _Software Metrics Symposium, 2001. METRICS 2001. Proceedings. Seventh International_ (pp. 124-134). IEEE.

Freedom of knowledge promotes knowledge sharing across dissimilar fields[^7], which has led to a number of success stories, such as Linux[^8]<sup>,</sup>[^9] and the free and open source software (FOSS) movement[^10]<sup>,</sup>[^11]. Motivation for development is higher when assistance from skilled developers is available[^12]<sup>,</sup>[^13] and results are more favorable[^14]<sup>,</sup>[^15]<sup>,</sup>[^16]<sup>,</sup>[^17]<sup>,</sup>[^18].
[^7]:Taylor, J.B., 1993, December. Discretion versus policy rules in practice. In _Carnegie-Rochester conference series on public policy_ (Vol. 39, pp. 195-214). North-Holland.
[^8]:Hertel, G., Niedner, S., Herrmann, S., 2003. Motivation of software developers in Open Source projects: an Internet-based survey of contributors to the Linux kernel. Research Policy, Open Source Software Development 32, 1159–1177. doi:10.1016/S0048-
7333(03)00047-7
[^9]:Lee, G.K., Cole, R.E., 2003. From a Firm-Based to a Community-Based Model of Knowledge Creation: The Case of the Linux Kernel Development. Organization Science 14, 633–649. doi:10.1287/orsc.14.6.633.24866
[^10]:Kogut, B., Metiu, A., 2001. Open Source Software Development and Distributed Innovation. Oxf Rev Econ Policy 17, 248–264. doi:10.1093/oxrep/17.2.248
[^11]:Mockus, A., Fielding, R.T., Herbsleb, J., 2000. A case study of open source software development: the Apache server, in: Proceedings of the 2000 International Conference on Software Engineering. ICSE 2000 the New Millennium. Presented at the Proceedings of the 2000 International Conference on Software Engineering. ICSE 2000 the New Millennium, pp. 263–272. doi:10.1145/337180.337209
[^12]:Bagozzi, R.P., Dholakia, U.M., 2006. Open Source Software User Communities: A Study of Participation in Linux User Groups. Management Science 52, 1099–1115. doi:10.1287/mnsc.1060.0545
[^13]:Dempsey, B.J., Weiss, D., Jones, P., Greenberg, J., 1999. A quantitative profile of a community of open source Linux developers. Chapel Hill: School of Information and Library Science, University of North Carolina
[^14]:Hars, A., Ou, S., 2001. Working for free? Motivations of participating in open source projects, in: Proceedings of the 34th Annual Hawaii International Conference on System Sciences. Presented at the Proceedings of the 34th Annual Hawaii International  Conference on System Sciences, p. 9 pp.-.doi:10.1109/HICSS.2001.927045
[^15]:Feller, J., Fitzgerald, B., others, 2002. Understanding open source software development. Addison-Wesley London.
[^16]:Bonaccorsi, A., Rossi, C., 2003. Why Open Source software can succeed. Research Policy, Open Source Software Development 32, 1243–1258. doi:10.1016/S0048-7333(03)00051-9f
[^17]:Crowston, K., Howison, J., 2005. The social structure of free and open source software development. First Monday 10. doi:10.5210/fm.v10i2.1207
[^18]:Tu, Q., 2000. Evolution in open source software: A case study. In _Software Maintenance, 2000. Proceedings. International Conference on_ (pp. 131-142). IEEE.

FOSS is thus now well established in mechatronics[^19]<sup>,</sup>[^20]<sup>,</sup>[^21], where its success has encouraged developers to adopt the model in Free and Open Source Hardware (FOSH)[^22]<sup>,</sup>[^23]<sup>,</sup>[^24]. Open hardware seen success with many projects including accessible microcontrollers like the Arduino[^25], which has been rapidly adopted by researchers in industry, academia, education, and citizen science[^26] for its ease of use, high modularization[^27], and affordable cost[^28]<sup>,</sup>[^29]<sup>,</sup>[^30].
[^19]:Drumea, A., 2012. Education in development of electronic modules using free and open source software tools. “HIDRAULICA” Magazine of Hydraulics, Pneumatics, Tribology, Ecology, Sensorics, Mechatronics, (3–4), pp.54-60.
[^20]:Sarker, M.O.F., Kim, C. and You, B.J., 2006, July. Development of a network-based real-time robot control system over IEEE 1394: Using open source software platform. In Mechatronics, 2006 IEEE International Conference on (pp. 563-568). IEEE.
[^21]:Xi, X.C., Hong, G.S. and Poo, A.N., 2010. Improving CNC contouring accuracy by integral sliding mode control. Mechatronics, 20(4), pp.442-452.
[^22]:Gibb, A., 2014. Building Open Source Hardware: DIY Manufacturing for Hackers and Makers. Pearson Education.
[^23]:Balka, K., Raasch, C., Herstatt, C., 2009. Open source enters the world of atoms: A statistical analysis of open design. First Monday 14. doi:10.5210/fm.v14i11.2670
[^24]:Zhang, L., Slaets, P. and Bruyninckx, H., 2012. An open embedded hardware and software architecture applied to industrial robot control. In Mechatronics and Automation (ICMA), 2012 International Conference on (pp. 1822-1828). IEEE.
[^25]:Banzi, M. and Shiloh, M., 2014. Getting Started with Arduino: The Open Source Electronics Prototyping Platform. Maker Media, Inc.
[^26]:Pearce, J.M., 2012. Building research equipment with free, open source hardware. Science, 337(6100), pp.1303-1304.
[^27]:Baldwin, C.Y., Clark, K.B., 2000. Design Rules: The power of modularity. MIT Press.
[^28]:Pocero, L., Amaxilatis, D., Mylonas, G., & Chatzigiannakis, I. (2017). Open source IoT meter devices for smart and energy-efficient school buildings. HardwareX, 1, 54-67.
[^29]:McMunn, M. S. (2017). A time-sorting pitfall trap and temperature datalogger for the sampling of surface-active arthropods. HardwareX, 1, 38-45.
[^30]:Oh, J., Hofer, R., & Fitch, W. T. (2017). An open source automatic feeder for animal experiments. HardwareX, 1, 13-21.

Scientists are increasingly using Arduino microcontrollers to automate mechatronic equipment in the lab[^27]<sup>,</sup>[^31], including wireless sensor networks, air pollution monitoring and control systems, data sharing and environmental monitoring, and oceanographic sensor and actuator control systems. Arduino microcontrollers have proven that a low-cost, open source platform has the capability to perform as well as much more expensive systems, making them popular as a teaching tool in mechatronics laboratories and for developing low-cost educational materials.
[^31]:Pearce, J.M., 2013. Open Source Lab: How to Build Your Own Hardware and Reduce Research Costs. Elsevier: New York.

Digital designs for 3-D printing can easily be open sourced, driving FOSH development. Rapid prototyping using 3-D printing has improved educational opportunities, reduced the cost of commonly used scientific equipment, and accelerated development. Much of modern experimental scientific research requires equipment with high acquisition and maintenance costs. With 3-D printed equipment, costs are reduced significantly as most parts can be printed and replaced internally.

One field benefiting from 3-D printing is optics, such as with embedded optical elements and interactive devices, flexure translation stages for open source microscopy, microscope stages, and a large, customizable library of mechanical components for optics setups. Open source 3-D printing has seen wide acceptance in biology and optics, but it remains conspicuously absent from the semiconductor arena.  

The solar photovoltaic industry, in particular requires reduced costs and quick manufacturing. There is a need to further develop this low-cost open source technology to characterize thin film anti-reflective coatings and TCOs for glass, mirror, and solar photovoltaics, whose transmission properties are angle dependent. Angular and spectral dependencies of reflectivity and transmissivity of materials are important parameters for current study, in x-ray and other spectroscopic analyses. Gimbal systems are often used to rotate samples in axes, to study this angular dependence of optical and optoelectronic properties.

The motivation for this study, then was to determine if a 3-D printed, FOSH system which reused component designs from other applications could be adequate as an optoelectronic measurement aid, while eliminating precision machining used in commercial systems, reducing equipment capital costs, and allowing complete control by the user for modifications and custom applications.

## References
Maybe references from the paper, in addition to useful links.
